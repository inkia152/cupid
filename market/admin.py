from django.contrib import admin

from market.models import Product, Shop, MainCategory, SubCategory, ProductDescription, PriceList


@admin.register(Shop)
class ShopAdmin(admin.ModelAdmin):
    model = Shop
    fields = ['name', 'address', 'is_active']


@admin.register(MainCategory)
class MainCategoryAdmin(admin.ModelAdmin):
    model = MainCategory
    fields = ['name', 'sort_index', 'is_active']


@admin.register(SubCategory)
class SubCategoryAdmin(admin.ModelAdmin):
    model = SubCategory
    fields = ['name', 'category', 'sort_index', 'is_active']


class ProductDescriptionInline(admin.StackedInline):
    model = ProductDescription
    fields = ['pack_type',
              'art_mgb',
              'name',
              'vat',
              'units',
              'price',
              'price_with_vat',
              'price_per_me',
              'price_per_me_with_vat',
              'img',
              ]
    extra = 1
    suit_classes = 'suit-tab suit-tab-metro'


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    model = Product
    fieldsets = [
        (
            'Основные характеристики',
            {
                'classes': ('suit-tab', 'suit-tab-general',),
                'fields': ('shop', 'category', 'is_active', 'sort_index'),
            }
        ),
    ]
    group_fieldsets = True

    inlines = [ProductDescriptionInline, ]
    suit_form_tabs = (
        ('general', 'Основные'),
        ('metro', 'Metro'),
    )
    list_filter = ['shop__name']
    search_fields = ['description_product__name']


@admin.register(PriceList)
class PriceListAdmin(admin.ModelAdmin):
    model = PriceList
    fields = ['shop', 'file_path']
