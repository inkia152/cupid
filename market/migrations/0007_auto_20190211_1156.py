# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-02-11 11:56
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('market', '0006_remove_product_category'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='subcategory',
            options={'ordering': ['sort_index'], 'verbose_name': 'Подкатегория', 'verbose_name_plural': 'Подкатегории'},
        ),
        migrations.RemoveField(
            model_name='product',
            name='main_category',
        ),
        migrations.AddField(
            model_name='product',
            name='category',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='product_sub_category', to='market.SubCategory', verbose_name='Подкатегория'),
        ),
    ]
