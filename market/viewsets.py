from rest_framework import viewsets, filters
from rest_framework.pagination import PageNumberPagination

from market.models import MainCategory, Product, SubCategory
from market.serializers import MainCategoryDetailSerializer, MainCategorySerializer, SubCategorySerializer, \
    SubCategoryDetailSerializer, ProductSerializer


class CursorSetPagination(PageNumberPagination):
    page_size = 300
    page_size_query_param = 'size'


class MainCategoryViewSet(viewsets.ModelViewSet):
    queryset = MainCategory.objects.all()
    serializer_class = MainCategorySerializer
    pagination_class = CursorSetPagination

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return MainCategoryDetailSerializer
        return super(MainCategoryViewSet, self).get_serializer_class()


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    pagination_class = CursorSetPagination
    filter_backends = (filters.SearchFilter, )
    search_fields = ('description_product__name', )

    def get_queryset(self):
        queryset = Product.objects.all()
        main_category = self.request.query_params.get('main_category', None)
        if main_category is not None:
            queryset = queryset.filter(category__category_id=main_category)
        category = self.request.query_params.get('category', None)
        if category is not None:
            queryset = queryset.filter(category_id=category)
        return queryset


class SubCategoryViewSet(viewsets.ModelViewSet):
    queryset = SubCategory.objects.all()
    serializer_class = SubCategorySerializer
    pagination_class = CursorSetPagination

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return SubCategoryDetailSerializer
        return super(SubCategoryViewSet, self).get_serializer_class()
