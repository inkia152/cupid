# coding: utf-8
import os
import datetime
from django.db import models
from django.db.models import ForeignKey, FileField
from django.db.models.signals import post_save
from django.dispatch import receiver

from config.common import MEDIA_ROOT
from config.utility import metro_price


def upload_to_price(instance, filename):
    name_date = "{}_{}".format(datetime.date.today(), filename)
    return os.path.join('media', 'projects', str(instance.shop.name), name_date)


class Shop(models.Model):
    name = models.CharField(verbose_name="Название магазина", max_length=128, null=True, blank=False)
    address = models.CharField(verbose_name="Адрес магазина", max_length=1024)
    is_active = models.BooleanField(verbose_name='Активный', default=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Магазин'
        verbose_name_plural = 'Магазины'


class Product(models.Model):
    """
    shop - fk
    """
    shop = ForeignKey('Shop', verbose_name="Магазин", related_name="product_shop")
    # main_category = ForeignKey('MainCategory', verbose_name="Категория", related_name='product_main_category',
    #                            null=True, blank=False)
    category = ForeignKey('SubCategory', verbose_name="Подкатегория", related_name='product_sub_category', null=True,
                          blank=True)
    is_active = models.BooleanField(verbose_name='Активный', default=True)
    sort_index = models.PositiveIntegerField(verbose_name='Индекс сортировки', default=0)

    def __str__(self):
        return self.description_product.name

    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'
        ordering = ['sort_index']

    @property
    def get_pack_type_kg(self):
        pack_type = self.description_product.pack_type
        if pack_type == 'килограм':
            return True


class ProductDescription(models.Model):
    product = models.OneToOneField('Product', verbose_name="Товар", related_name='description_product', null=True, blank=False)
    pack_type = models.CharField(verbose_name="Тип упаковки", max_length=2048, null=True, blank=False)
    art_mgb = models.CharField(verbose_name="Артикул", max_length=2048, null=True, blank=False)
    name = models.CharField(verbose_name="Название", max_length=2048, null=True, blank=False)
    vat = models.CharField(verbose_name="НДС", max_length=2048, null=True, blank=False)
    units = models.CharField(verbose_name="Количество в упаковке", max_length=2048, null=True, blank=False)
    price = models.DecimalField(max_digits=10, decimal_places=2, default=0, verbose_name="Цена без НДС")
    price_with_vat = models.DecimalField(max_digits=10, decimal_places=2, default=0, verbose_name="Цена с НДС")
    price_per_me = models.DecimalField(max_digits=10, decimal_places=2, default=0,
                                       verbose_name="Цена за упаковку без НДС")
    price_per_me_with_vat = models.DecimalField(max_digits=10, decimal_places=2, default=0,
                                                verbose_name="Цена за упаковку c НДС")
    img = models.FilePathField(verbose_name='Изображение', null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Описание товара'
        verbose_name_plural = 'Описание товаров'


class MainCategory(models.Model):
    name = models.CharField(verbose_name="Название", max_length=2048, null=True, blank=False)
    is_active = models.BooleanField(verbose_name='Активный', default=True)
    sort_index = models.PositiveIntegerField(verbose_name='Индекс сортировки', default=0)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Главная категория'
        verbose_name_plural = 'Главные категории'
        ordering = ['sort_index']


class SubCategory(models.Model):
    category = ForeignKey('MainCategory', verbose_name="Категория", related_name='main_cat_subcategories',
                          null=True, blank=False)
    name = models.CharField(verbose_name="Название", max_length=2048, null=True, blank=False)
    is_active = models.BooleanField(verbose_name='Активный', default=True)
    sort_index = models.PositiveIntegerField(verbose_name='Индекс сортировки', default=0)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Подкатегория'
        verbose_name_plural = 'Подкатегории'
        ordering = ['sort_index']


class PriceList(models.Model):
    shop = ForeignKey('Shop', verbose_name="Магазин", related_name="pricelist_shop")
    file_path = FileField(upload_to=upload_to_price)
    created_date = models.DateTimeField(verbose_name='Дата создания', blank=True, null=True, auto_now=True)

    class Meta:
        verbose_name = 'Прайс лист'
        verbose_name_plural = 'Прайс листы'

    def __str__(self):
        return str(self.file_path).split('/')[-1]


@receiver(post_save, sender=PriceList)
def update_price(instance, **kwargs):
    shop = instance.shop
    file_path = os.path.join(MEDIA_ROOT, str(instance.file_path))
    print(file_path)
    metro_price(shop, file_path)
