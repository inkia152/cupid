from rest_framework import serializers

from market.models import MainCategory, SubCategory, Product, ProductDescription


class SubCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = SubCategory
        fields = ['id', 'name', 'is_active', 'sort_index']


class MainCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = MainCategory
        fields = ['id', 'name', 'is_active', 'sort_index']


class MainCategoryDetailSerializer(serializers.ModelSerializer):
    main_cat_subcategories = SubCategorySerializer(many=True, required=False)
    # count = pass

    class Meta:
        model = MainCategory
        fields = ['id', 'name', 'is_active', 'sort_index', 'main_cat_subcategories']


class ProductDescriptionSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductDescription
        fields = ['id',
                  'pack_type',
                  'art_mgb',
                  'name',
                  'vat',
                  'units',
                  'price',
                  'price_with_vat',
                  'price_per_me',
                  'price_per_me_with_vat',
                  'img',
                  ]


class ProductSerializer(serializers.ModelSerializer):
    description_product = ProductDescriptionSerializer(required=False)

    class Meta:
        model = Product
        fields = ['id', 'category', 'shop', 'description_product', 'is_active', 'sort_index']


class ProductOrderSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = ['id', 'category', 'shop', 'is_active', 'sort_index']


class SubCategoryDetailSerializer(serializers.ModelSerializer):
    product_sub_category = ProductSerializer(many=True, required=False)

    def to_internal_value(self, data):
        self.fields['count'] = 1
        return super(SubCategoryDetailSerializer, self).to_internal_value(data)

    class Meta:
        model = SubCategory
        fields = ['id', 'name', 'is_active', 'sort_index', 'product_sub_category']
