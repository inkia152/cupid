# coding: utf-8
import datetime

import jwt
from django.contrib.auth import authenticate
from django.core.mail import EmailMessage
from django.http import HttpResponse
from django.template.loader import get_template
from hashid_field.rest import HashidSerializerCharField
from rest_framework import serializers
from rest_framework_jwt.serializers import jwt_payload_handler

from config.local import DEBUG
from config.production import EMAIL_HOST_PASSWORD
from market.serializers import ProductSerializer
from order.models import Cart, Order, OrderProduct
from user.models import ApiUser, Address, create_user_log


def email_view_admit(user):
    print('Подтверждение, отправка письма')
    subject = "Вы успешно зарегистрировались!"
    send_to = [user.email]
    from_email = 'kupivdom.info@gmail.com'

    ctx = {
        'user': user,
        'user_email': user.email,
        'link': 'http://kupivdom.mobi/api/user/{}/activate/'.format(user.reference_id)
    }

    message = get_template('email/confirm_email.html').render(ctx)
    director_msg = EmailMessage(
        subject,
        message,
        to=send_to,
        from_email=from_email
    )
    director_msg.content_subtype = 'html'
    director_msg.send()

    return HttpResponse({'response': 1})


def email_new_password(user, password):
    print('Отправка нового пароля')
    subject = "Ваш новый пароль"
    send_to = [user.email]
    from_email = 'kupivdom.info@gmail.com'

    ctx = {
        'user': user,
        'password': password
    }

    message = get_template('email/forgot_password_email.html').render(ctx)
    director_msg = EmailMessage(
        subject,
        message,
        to=send_to,
        from_email=from_email
    )
    director_msg.encoding = "utf-8"
    director_msg.content_subtype = 'html'
    director_msg.send()

    return HttpResponse({'response': 1})


class RegistrationSerializer(serializers.ModelSerializer):
    """Serializers registration requests and creates a new user."""

    # Ensure passwords are at least 8 characters long, no longer than 128
    # characters, and can not be read by the client.
    password = serializers.CharField(
        max_length=128,
        min_length=1,
        write_only=True
    )

    class Meta:
        model = ApiUser
        # List all of the fields that could possibly be included in a request
        # or response, including fields specified explicitly above.
        fields = ['id', 'email', 'password', 'phone']

    def create(self, validated_data):
        # Use the `create_user` method we wrote earlier to create a new user.
        user = ApiUser.objects.create_user(**validated_data)
        user.reference_id = user.id
        user.save()
        if not DEBUG:
            email_view_admit(user)
        create_user_log(user, 'register')
        return user


class LoginSerializer(serializers.Serializer):
    email = serializers.CharField(max_length=255)
    password = serializers.CharField(max_length=128, write_only=True)
    token = serializers.CharField(max_length=255, read_only=True)

    def validate(self, data):
        # The `validate` method is where we make sure that the current
        # instance of `LoginSerializer` has "valid". In the case of logging a
        # user in, this means validating that they've provided an email
        # and password and that this combination matches one of the users in
        # our database.
        email = data.get('email', None)
        password = data.get('password', None)

        # Raise an exception if an
        # email is not provided.
        if email is None:
            raise serializers.ValidationError(
                'Email обязателен для входа'
            )

        # Raise an exception if a
        # password is not provided.
        if password is None:
            raise serializers.ValidationError(
                'Пароль обязателен для входа.'
            )

        # The `authenticate` method is provided by Django and handles checking
        # for a user that matches this email/password combination. Notice how
        # we pass `email` as the `username` value since in our User
        # model we set `USERNAME_FIELD` as `email`.
        user = authenticate(username=email, password=password)
        # login(self.request, user)

        # If no user was found matching this email/password combination then
        # `authenticate` will return `None`. Raise an exception in this case.
        if user is None:
            # return {"error": "Пользователь с таким email не найден"}
            raise serializers.ValidationError(
                'Пользователь с таким email и паролем не найден.'
            )

        # Django provides a flag on our `User` model called `is_active`. The
        # purpose of this flag is to tell us whether the user has been banned
        # or deactivated. This will almost never be the case, but
        # it is worth checking. Raise an exception in this case.
        if not user.is_active:
            raise serializers.ValidationError(
                'Этот пользователь был деактивирован.'
            )

        # The `validate` method should return a dictionary of validated data.
        # This is the data that is passed to the `create` and `update` methods
        # that we will see later on.
        user.last_login = datetime.datetime.now()
        payload = jwt_payload_handler(user)
        token = jwt.encode(payload, str(user.jwt_secret))
        user.save()
        Cart.objects.get_or_create(user=user)
        create_user_log(user, 'login')
        return {
            'email': user.email,
            'token': token.decode('utf-8')
        }


class AddressUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = [
            'id',
            'city',
            'street',
            'house',
            'building',
            'apartment',
            'comment',
        ]


class OrderProductUserSerializer(serializers.ModelSerializer):
    price = serializers.DecimalField(read_only=True, max_digits=10, decimal_places=2)
    product = ProductSerializer()

    class Meta:
        model = OrderProduct
        fields = ['id', 'product', 'count', 'price', 'created_date', 'active', 'cart', 'order']


class OrderUserSerializer(serializers.ModelSerializer):
    # total_price = serializers.DecimalField(required=False, max_digits=10, decimal_places=2)
    # total_count = serializers.IntegerField(required=False)
    order_products = OrderProductUserSerializer(many=True, required=False)

    class Meta:
        model = Order
        fields = [
            'id',
            'order_status',
            'order_products',
            'total_price',
            'total_count',
            'created_date',
            'updated_date',
            'favourite',
            'name'
        ]


class ApiUserSerializer(serializers.ModelSerializer):
    user_address = AddressUserSerializer(required=False, many=True)
    reference_id = HashidSerializerCharField(source_field='user.ApiUser.reference_id')
    user_orders = OrderUserSerializer(required=False, many=True)
    favourite_orders = serializers.SerializerMethodField()

    class Meta:
        model = ApiUser
        fields = [
            'id',
            'reference_id',
            'user_address',
            'email',
            'phone',
            'is_active',
            'is_admin',
            'password',
            'last_login',
            'user_orders',
            'favourite_orders',
        ]
        extra_kwargs = {'password': {'write_only': True}}

    def get_favourite_orders(self, obj):
        orders = Order.objects.filter(user=obj, favourite=True).all()
        serializer = OrderUserSerializer(instance=orders, many=True, required=False, context=self.context)
        return serializer.data


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = [
            'id',
            'user',
            'city',
            'street',
            'house',
            'building',
            'apartment',
            'comment',
            'favourite',
        ]
