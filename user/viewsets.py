from django.contrib.auth.hashers import make_password
from django.db import IntegrityError
from django.http import HttpResponse, Http404
from django.template import loader
from rest_framework import mixins, viewsets, status
from rest_framework.decorators import action
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from user.models import ApiUser, create_user_log, Address
from user.permissions import UserPermission
from user.serializers import ApiUserSerializer, AddressSerializer


class UserViewSet(mixins.RetrieveModelMixin, mixins.ListModelMixin, mixins.UpdateModelMixin, GenericViewSet):
    queryset = ApiUser.objects.all()
    serializer_class = ApiUserSerializer
    permission_classes = (UserPermission,)
    lookup_field = 'reference_id'

    def get_queryset(self):
        return ApiUser.objects.filter(id=self.request.user.id)

    @action(detail=True, methods=['post'])
    def updates(self, request, pk=None, reference_id=None):
        lookup = {'reference_id': reference_id} if pk is None else {'pk': pk}
        user = get_object_or_404(ApiUser, **lookup)
        if request.data.get('password'):
            user.password = make_password(request.data.get('password'))
        elif request.data.get('email'):
            user.email = request.data.get('email')
        elif request.data.get('phone'):
            user.phone = request.data.get('phone')
        try:
            user.save()
        except IntegrityError:
            return Response({
                "errors": [{"internal": ["Такой телефон или емейл уже существует"]}],
                "status_code": 400

            })

        return Response(ApiUserSerializer(instance=user).data)

    @action(detail=True, methods=['get'])
    def activate(self, request, pk=None, reference_id=None):
        lookup = {'reference_id': reference_id} if pk is None else {'pk': pk}
        op = get_object_or_404(ApiUser, **lookup)
        op.is_active = True
        op.save()

        create_user_log(op, 'activate')
        context = ApiUserSerializer(instance=op).data
        template = loader.get_template("submission.html")
        return HttpResponse(template.render(context, request))


class AddressViewSet(viewsets.ModelViewSet):
    queryset = Address.objects.all()
    serializer_class = AddressSerializer
    permission_classes = (AllowAny,)

    def get_queryset(self):
        return Address.objects.filter(user=self.request.user)
