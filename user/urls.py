from django.conf.urls import url
from rest_framework_jwt.views import refresh_jwt_token, obtain_jwt_token, verify_jwt_token

from user.views import RegistrationAPIView, LoginAPIView, forget_password, LogoutAPIView

urlpatterns = [
    url(r'^api-token-refresh/', refresh_jwt_token),
    url(r'^api-token-auth/', obtain_jwt_token),
    url(r'^api-token-verify/', verify_jwt_token),
    url(r'^user_register/', RegistrationAPIView.as_view()),
    url(r'^user_login/?$', LoginAPIView.as_view()),
    url(r'^user_logout/?$', LogoutAPIView.as_view()),
    url(r'^user_forget/$', forget_password, name='forget_password'),

]
