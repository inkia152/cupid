from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.utils import timezone
from hashid_field import HashidField
import uuid


class ApiUserManager(BaseUserManager):
    def create_user(self, email, phone, password=None):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('Users must have an email address')
        user = self.model(
            email=self.normalize_email(email),
        )
        user.is_active = False
        user.phone = phone
        user.set_password(password)
        user.save(using=self._db)
        user.reference_id = user.id
        user.save()
        return user

    def create_superuser(self, email, phone, password):
        """
        Creates and saves a superuser with the given email and
        password.
        """
        user = self.create_user(
            email,
            password=password,
            phone=phone
        )
        user.is_admin = True
        user.save(using=self._db)
        user.reference_id = user.id
        user.save()
        return user


class ApiUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(verbose_name='email address', max_length=255, unique=True)
    phone = models.CharField(verbose_name='Телефон', max_length=200, unique=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    date_joined = models.DateTimeField(default=timezone.now)
    reference_id = HashidField(allow_int_lookup=True, blank=True, null=True)
    jwt_secret = models.UUIDField(default=uuid.uuid4)

    objects = ApiUserManager()

    USERNAME_FIELD = 'email'
    EMAIL_FIELD = 'email'
    REQUIRED_FIELDS = ['phone', ]

    class Meta:
        verbose_name = 'Юзер'
        verbose_name_plural = 'Юзеры'

    def __str__(self):
        return self.email

    @property
    def is_staff(self):
        return self.is_admin

    @property
    def is_superuser(self):
        return self.is_admin

    def get_short_name(self):
        return self.phone

    def get_full_name(self):
        return self.phone


def jwt_get_secret_key(user_model):
    return user_model.jwt_secret


class Address(models.Model):
    user = models.ForeignKey(ApiUser, verbose_name='Юзер', null=False, related_name='user_address')
    city = models.CharField(verbose_name='Город', null=False, max_length=256)
    street = models.CharField(verbose_name='Улица', null=False, max_length=256)
    house = models.CharField(verbose_name='Дом', null=False, max_length=256)
    building = models.CharField(verbose_name='Корпус', max_length=256, null=True, blank=True)
    apartment = models.CharField(verbose_name='Квартира', max_length=256, null=True, blank=True)
    comment = models.CharField(verbose_name='Примечание', max_length=256, null=True, blank=True)
    favourite = models.BooleanField(verbose_name='Выбранный адрес', default=True)

    class Meta:
        verbose_name = 'Адрес'
        verbose_name_plural = 'Адреса'


class UserLog(models.Model):
    TYPES = (
        ('register', 'Регистрация'),
        ('login', 'Вход'),
        ('logout', 'Выход'),
        ('activate', 'Подтвержден'),
        ('forget', 'Восстановил пароль'),
    )
    type = models.CharField('Тип записи', choices=TYPES, max_length=10, null=False, blank=False)
    user = models.ForeignKey(ApiUser, verbose_name='Юзер', null=False, related_name='user_log')
    comment = models.CharField(verbose_name='Примечание', max_length=256, null=True, blank=True)
    date = models.DateTimeField(verbose_name='Дата создания', default=timezone.now)

    class Meta:
        verbose_name = 'Лог юзера'
        verbose_name_plural = 'Логи юзера'


def create_user_log(user, type, comment=None):
    log = UserLog(
        user=user,
        type=type,
        comment=comment,
    )
    log.save()
    return log
