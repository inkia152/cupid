# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-01-21 06:32
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0007_apiuser_reference_id'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserLog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type', models.CharField(choices=[('register', 'Регистрация'), ('login', 'Вход'), ('logout', 'Выход'), ('activate', 'Подтвержден')], max_length=10, verbose_name='Тип записи')),
                ('comment', models.CharField(blank=True, max_length=256, null=True, verbose_name='Примечание')),
                ('date', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Дата создания')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='user_log', to=settings.AUTH_USER_MODEL, verbose_name='Юзер')),
            ],
            options={
                'verbose_name': 'Лог юзера',
                'verbose_name_plural': 'Логи юзера',
            },
        ),
    ]
