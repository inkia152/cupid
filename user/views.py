import json
import uuid

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from rest_framework import status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from strgen import StringGenerator as SG

from user.models import ApiUser, create_user_log
from user.serializers import RegistrationSerializer, LoginSerializer, email_new_password


class RegistrationAPIView(APIView):
    # Allow any user (authenticated or not) to hit this endpoint.
    permission_classes = (AllowAny,)
    serializer_class = RegistrationSerializer

    def post(self, request):
        # The create serializer, validate serializer, save serializer pattern
        # below is common and you will see it a lot throughout this course and
        # your own work later on. Get familiar with it.
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_201_CREATED)


class LoginAPIView(APIView):
    permission_classes = (AllowAny,)
    serializer_class = LoginSerializer

    def post(self, request):
        # Notice here that we do not call `serializer.save()` like we did for
        # the registration endpoint. This is because we don't  have
        # anything to save. Instead, the `validate` method on our serializer
        # handles everything we need.
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        return Response(serializer.data, status=status.HTTP_200_OK)


class LogoutAPIView(APIView):
    """
    Use this endpoint to log out all sessions for a given user.
    """
    permission_classes = (IsAuthenticated, )

    def post(self, request, *args, **kwargs):
        user = request.user
        user.jwt_secret = uuid.uuid4()
        user.save()
        create_user_log(user, 'logout')
        return Response(status=status.HTTP_200_OK)


@require_http_methods(["POST"])
@csrf_exempt
def forget_password(request):
    json_data = json.loads(request.body.decode("utf-8"))
    email = request.POST.get('email', 0) or json_data.get('email', 0)
    if email:
        user = ApiUser.objects.filter(email=email).first()
        if user:
            password = SG("[\l\d]{10}").render()
            user.set_password(password)
            user.save()
            email_new_password(user, password)
            create_user_log(user, 'forget')
            return JsonResponse({'resp': 'Send post to user email with new password'})
        else:
            return JsonResponse({'resp': 'No user with such email'})
    return JsonResponse({'resp': 'No email'})
