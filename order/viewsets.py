from datetime import datetime

from rest_framework import mixins, status
from rest_framework.decorators import action
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from order.models import Order, Cart, OrderProduct
from order.serializers import OrderSerializer, CartSerializer, OrderProductSerializer
from user.models import Address


class OrderViewSet(mixins.RetrieveModelMixin, mixins.ListModelMixin, mixins.CreateModelMixin, GenericViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = (IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        cart = Cart.objects.filter(user=self.request.user).first()
        order_products = OrderProduct.objects.filter(cart=cart, active=True).all()
        if not self.request.user.user_address.exists():
            return Response({'error': [{'order': 'У пользователя нет адреса'}]})

        address = Address.objects.filter(user_id=self.request.user, favourite=True).first()
        if order_products.exists():
            self.perform_create(serializer, cart=cart, order_products=order_products, user=self.request.user,
                                address=address)
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        else:
            return Response({"errors": [{"order": "Нет товаров в корзине"}]})

    def perform_create(self, serializer, **kwargs):
        status_code = 'create'
        cart = kwargs.get('cart')
        user = kwargs.get('user')
        address = kwargs.get('address')
        order_products = kwargs.get('order_products')

        obj = serializer.save(order_status=status_code, user=user, address=address)
        for order in order_products:
            order.created_at = datetime.now()
            order.save()
        order_products.update(cart=None, order=obj)
        cart.save()
        obj.save()
        OrderProduct.objects.filter(cart=cart, active=False).delete()
        return Response(obj.id)

    @action(detail=True, methods=['post'])
    def favourite(self, request, pk=None):
        order = get_object_or_404(Order, pk=pk)
        if order.favourite is False:
            order.favourite = True
        else:
            order.favourite = False
        order.save()
        return Response(OrderSerializer(instance=order).data)

    @action(detail=True, methods=['post'])
    def rename(self, request, pk=None):
        order = get_object_or_404(Order, pk=pk)
        order.name = request.data.get('name')
        order.save()
        return Response(OrderSerializer(instance=order).data)

    @action(detail=True, methods=['post'])
    def reorder(self, request, pk=None):
        order = get_object_or_404(Order, pk=pk)
        cart = Cart.objects.filter(user=self.request.user).first()
        products = OrderProduct.objects.filter(
            order=order,
        ).all()
        for i in products:
            order_product = OrderProduct(
                cart=cart,
                product=i.product,
                count=i.count,
                active=True
            )
            order_product.save()
        cart.save()
        return Response(CartSerializer(instance=cart).data)


class CartViewSet(mixins.RetrieveModelMixin, mixins.ListModelMixin, GenericViewSet):
    queryset = Cart.objects.all()
    serializer_class = CartSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return Cart.objects.filter(user=self.request.user)


class OrderProductViewSet(mixins.RetrieveModelMixin,
                          mixins.ListModelMixin,
                          mixins.CreateModelMixin,
                          mixins.UpdateModelMixin,
                          mixins.DestroyModelMixin,
                          GenericViewSet):
    queryset = OrderProduct.objects.all()
    serializer_class = OrderProductSerializer

    permission_classes = (IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        cart = Cart.objects.filter(user=self.request.user).first()
        old_product = OrderProduct.objects.filter(
            cart=cart,
            product_id=self.request.data['product']
        ).first()
        if old_product:
            if not old_product.active:
                old_product.active = True
                old_product.count = self.request.data['count']
            else:
                old_product.count += int(self.request.data['count'])
            old_product.save()
            cart.save()
            return Response(OrderProductSerializer(instance=old_product).data)
        else:
            cart.save()
            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    @action(detail=True, methods=['post'])
    def inc(self, request, pk=None):
        cart = Cart.objects.filter(user=self.request.user).first()
        op = get_object_or_404(OrderProduct, pk=pk)
        count = op.count
        if op.active is False:
            op.active = True
        if op.product.get_pack_type_kg:
            op.count = count + 200
        else:
            op.count = count + 1
        op.save()
        cart.save()
        return Response(OrderProductSerializer(instance=op).data)

    @action(detail=True, methods=['post'])
    def dec(self, request, pk=None):
        cart = Cart.objects.filter(user=self.request.user).first()
        op = get_object_or_404(OrderProduct, pk=pk)
        count = op.count

        if op.product.get_pack_type_kg:
            if count > 200:
                op.count = count - 200
            elif count == 200:
                op.count = 0
                op.active = False
        else:
            if count > 1:
                op.count = count - 1
            elif count == 1:
                op.count = 0
                op.active = False
        op.save()
        cart.save()
        return Response(OrderProductSerializer(instance=op).data)

    @action(detail=True, methods=['post'])
    def deactivate(self, request, pk=None):
        cart = Cart.objects.filter(user=self.request.user).first()
        op = get_object_or_404(OrderProduct, pk=pk)
        op.active = False
        op.save()
        cart.save()
        return Response(OrderProductSerializer(instance=op).data)

    @action(detail=True, methods=['post'])
    def activate(self, request, pk=None):
        cart = Cart.objects.filter(user=self.request.user).first()
        op = get_object_or_404(OrderProduct, pk=pk)
        op.active = True
        op.save()
        cart.save()
        return Response(OrderProductSerializer(instance=op).data)
