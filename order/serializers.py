from rest_framework import serializers

from market.models import Product
from market.serializers import ProductSerializer
from order.models import OrderProduct, Cart, Order
from user.models import Address
from user.serializers import AddressSerializer


class OrderProductSerializer(serializers.ModelSerializer):
    price = serializers.DecimalField(read_only=True, max_digits=10, decimal_places=2)
    product = ProductSerializer()

    class Meta:
        model = OrderProduct
        fields = ['id', 'product', 'count', 'price', 'created_date', 'active', 'cart', 'order']

    def to_internal_value(self, data):
        self.fields['product'] = serializers.PrimaryKeyRelatedField(queryset=Product.objects.all())
        return super(OrderProductSerializer, self).to_internal_value(data)

    def create(self, validated_data):
        orderproducts = OrderProduct(**validated_data)

        request = self.context['request']
        user = request.user
        cart = Cart.objects.filter(user=user).first()
        orderproducts.cart = cart
        orderproducts.active = True
        orderproducts.save()
        cart.save()

        return orderproducts


class CartSerializer(serializers.ModelSerializer):
    cart_products = OrderProductSerializer(many=True, required=False)

    class Meta:
        model = Cart
        fields = ['id', 'price', 'total_count', 'cart_products', 'user', 'create_date']


class OrderSerializer(serializers.ModelSerializer):
    total_price = serializers.DecimalField(required=False, max_digits=10, decimal_places=2)
    delivery_price = serializers.DecimalField(required=False, max_digits=10, decimal_places=2)
    total_count = serializers.IntegerField(required=False)
    order_products = OrderProductSerializer(many=True, required=False)
    address = AddressSerializer()

    class Meta:
        model = Order
        fields = [
            'id',
            'order_status',
            'order_products',
            'total_price',
            'total_count',
            'user',
            'address',
            'created_date',
            'updated_date',
            'order_status',
            'delivery_time',
            'payment_type',
            'delivery_price',
            'name',
            'favourite'
        ]

    def to_internal_value(self, data):
        self.fields['address'] = serializers.PrimaryKeyRelatedField(queryset=Address.objects.all(),required=False)
        return super(OrderSerializer, self).to_internal_value(data)
