from decimal import Decimal

from django.db import models
from django.db.models import Sum, Count
from django.utils import timezone

from market.models import Product


class Order(models.Model):
    STATUSES = (
        ('create', 'Принят'),
        ('waiting_delivery', 'Ожидает поставки'),
        ('complete', 'Выполнен'),
    )
    TIME_DELIVERY = (
        ('morning', '11-12'),
        ('evening1', '18-19'),
        ('evening2', '19-20'),
    )
    PAYMENT = (
        ('cash', 'Наличные'),
        ('card', 'Карта')
    )
    order_status = models.CharField('Статус заказа', choices=STATUSES, max_length=30, null=False, blank=True)
    user = models.ForeignKey('user.ApiUser', verbose_name='Пользователь', related_name='user_orders', blank=False, null=True)
    address = models.ForeignKey('user.Address', verbose_name='Адрес', related_name='address_order', blank=False, null=True)
    total_price = models.DecimalField(verbose_name='Cумма заказа', max_digits=10, decimal_places=2, default=0, null=True)
    total_count = models.IntegerField(verbose_name='Общее количество продуктов', blank=True, null=True, default=0)
    created_date = models.DateTimeField(verbose_name='Дата создания', blank=True, null=True)
    updated_date = models.DateTimeField(verbose_name='Дата обновления', blank=True, null=True)
    delivery_time = models.CharField('Время доставки', choices=TIME_DELIVERY, max_length=30, null=False, blank=False)
    payment_type = models.CharField('Тип оплаты', choices=PAYMENT, max_length=30, null=False, blank=False)
    delivery_price = models.DecimalField(verbose_name='Cумма заказа', max_digits=10, decimal_places=2, default=0, null=True)
    name = models.CharField(verbose_name='Название заказа', null=True, max_length=256)
    favourite = models.BooleanField(verbose_name='Любимый заказ', default=False)

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'
        ordering = ['-id']

    def __str__(self):
        return 'Заказ #{}'.format(self.id)

    def save(self, *args, **kwargs):
        """On save, update timestamps"""
        if not self.id:
            self.created_date = timezone.now()
        self.updated_date = timezone.now()
        self.total_price = self.get_total()
        self.total_count = self.get_total_count()
        self.delivery_price = self.get_delivery_price()
        super(Order, self).save(*args, **kwargs)

    def get_total(self):
        return float(OrderProduct.objects.filter(order=self, active=True).aggregate(sum=Sum('price'))['sum'] or 0)

    def get_total_count(self):
        return float(OrderProduct.objects.filter(order=self, active=True).aggregate(sum=Sum('count'))['sum'] or 0)

    def get_delivery_price(self):
        count = OrderProduct.objects.filter(order=self, active=True).\
            aggregate(count=Count('product__shop_id', distinct=True))['count']
        price = 500
        if count > 1:
            price += (count-1) * 300
        return float(price)


class OrderProduct(models.Model):
    order = models.ForeignKey(Order, verbose_name='Заказанный товар', related_name='order_products', blank=True, null=True)
    cart = models.ForeignKey('Cart', verbose_name='Корзина', related_name='cart_products', blank=True, null=True)
    product = models.ForeignKey('market.Product', verbose_name='Товар', null=True)
    price = models.DecimalField(verbose_name='Цена заказа', max_digits=10, decimal_places=2, default=0)
    count = models.PositiveIntegerField(verbose_name='Количество')
    created_date = models.DateTimeField(verbose_name='Дата создания', blank=True, null=True, auto_now=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return 'Заказанный товар "{}" для заказа #{}'.format(self.product.description_product.name, self.order_id)

    class Meta:
        verbose_name = 'Заказаннай товар'
        verbose_name_plural = 'Заказанные товары'
        ordering = ['id']

    def save(self, *args, **kwargs):
        product = Product.objects.filter(id=self.product_id).first()
        if product:
            price = product.description_product.price_per_me_with_vat
        else:
            return {'error': [{'order_product': 'Нет такого продукта в системе'}]}

        self.price = Decimal(price) * self.count
        if product.get_pack_type_kg:
            self.price = self.price / Decimal(1000)
        super(OrderProduct, self).save(*args, **kwargs)


class Cart(models.Model):
    user = models.ForeignKey('user.ApiUser', verbose_name='Пользователь', related_name='cart_user', blank=True, null=True)
    price = models.DecimalField(verbose_name='Сумма заказа', max_digits=10, decimal_places=2, default=0)
    total_count = models.PositiveIntegerField(verbose_name='Общее количество товаров', blank=True, null=True)
    create_date = models.DateTimeField(verbose_name='Дата создания', blank=True, null=True, auto_now=True)

    class Meta:
        verbose_name = 'Корзина'
        verbose_name_plural = 'Корзины'
        ordering = ['id']

    def save(self, *args, **kwargs):
        self.price = self.get_price()
        self.total_count = self.get_total_count()
        super(Cart, self).save(*args, **kwargs)

    def get_price(self):
        return float(OrderProduct.objects.filter(cart=self, active=True).aggregate(sum=Sum('price'))['sum'] or 0)

    def get_total_count(self):
        return float(OrderProduct.objects.filter(cart=self, active=True).aggregate(sum=Sum('count'))['sum'] or 0)
