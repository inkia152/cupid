from .common import *  # noqa

DEBUG = False
ALLOWED_HOSTS = ['116.202.31.143', ]
EMAIL_HOST = 'imap.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'kupivdom.info@gmail.com'
EMAIL_HOST_PASSWORD = os.getenv('EMAIL_PASSWORD')
EMAIL_USE_SSL = False
EMAIL_USE_TLS = True

SECRET_KEY = os.getenv('SECRET_KEY')
SESSION_COOKIE_SECURE = False
SECURE_BROWSER_XSS_FILTER = True
# SECURE_CONTENT_TYPE_NOSNIFF = True
# SECURE_HSTS_INCLUDE_SUBDOMAINS = True
# SECURE_HSTS_SECONDS = 31536000
# SECURE_REDIRECT_EXEMPT = []
# SECURE_SSL_REDIRECT = True
# SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'level': 'ERROR',
            'class': 'logging.StreamHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['console'],
            'level': 'ERROR',
            'propagate': True,
        }
    }
}

DATABASES = {
    'default': {
       'ENGINE': 'django.db.backends.postgresql_psycopg2',
       'NAME': 'cupid',
       'PASSWORD': os.getenv('DB_PASSWORD'),
       'HOST': 'localhost',
       'USER': 'cupid_user',
   }
}
