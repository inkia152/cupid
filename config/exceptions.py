from django.http import Http404
from rest_framework.exceptions import NotAuthenticated
from rest_framework.views import exception_handler


def custom_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)

    if response is not None:
        # check if exception is NotAuthenticated (no items)
        if isinstance(exc, NotAuthenticated):
            response.data = {'errors': [{'authenticate': "Пользователь не аутентифицирован"}]}
        if isinstance(exc, Http404):
            response.data = {'errors': [{'object': "Объект не найден"}]}
            # serve status code in the response
            response.data['status_code'] = response.status_code
            return response

        # check if exception has dict items
        if hasattr(exc.detail, 'items'):
            # remove the initial value
            response.data = {}
            errors = []
            for key, value in exc.detail.items():
                # append errors into the list
                # errors.append("{} : {}".format(key, " ".join(value)))
                errors.append({key: value})

            # add property errors to the response
            response.data['errors'] = errors
        # serve status code in the response
        response.data['status_code'] = response.status_code

    return response