from rest_framework.routers import DefaultRouter

from market.viewsets import MainCategoryViewSet, SubCategoryViewSet, ProductViewSet
from order.viewsets import CartViewSet, OrderViewSet, OrderProductViewSet
from user.viewsets import UserViewSet, AddressViewSet

router = DefaultRouter()
router.register(r'user', UserViewSet)
router.register(r'address', AddressViewSet)

router.register(r'category', MainCategoryViewSet)
router.register(r'sub_category', SubCategoryViewSet)
router.register(r'product', ProductViewSet)

router.register(r'cart', CartViewSet)
router.register(r'order', OrderViewSet)
router.register(r'order_product', OrderProductViewSet)
