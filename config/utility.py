# coding: utf-8
import os

import time
from openpyxl import load_workbook
from config.common import BASE_DIR


def metro_price(shop, file):
    """
    Load xsl file to database
    :param file: path to file
    :param shop: Shop instance
    :return:
    0 Main Category,
    1 Pack Type,
    2 Art MGB,
    3 Art Name,
    4 VAT,
    5 Units in ME,
    6 Price per unit,
    7 Price per unit with VAT,
    8 Price per ME,
    9 Price per ME with VAT
    """
    from market.models import Product, ProductDescription, MainCategory, SubCategory
    start_time = time.time()
    wb = load_workbook(file)
    ws = wb[wb.sheetnames[0]]
    max_row = ws.max_row
    print(max_row)
    # import pdb;pdb.set_trace()
    for row in ws.iter_rows(min_row=2, max_row=max_row):
        # создаем временный тупль чтобы в нем значения None поменять на пустую строку
        temp_row = row
        for cell in temp_row:
            if cell.value is None:
                cell.value = ''
        # Пытаемся получить объект описания товара по артиклю и названию
        try:
            product_desc = ProductDescription.objects.get(art_mgb=temp_row[2].value,
                                                          name=temp_row[3].value.lower().rstrip())
        except ProductDescription.DoesNotExist:
            # Если такого описания не существует, соотвественно нет и самого объекта товара
            try:
                # Получаем подкатегорию
                sub_cat = SubCategory.objects.get(name=temp_row[0].value.lower().rstrip())
            except SubCategory.DoesNotExist:
                # Если такой подгатегории нет, то создаем ее и присваиваем ей главную категорию - прочее
                main_cat = MainCategory.objects.get(name='прочее')
                sub_cat = SubCategory(name=temp_row[0].value.lower().rstrip(),
                                      category=main_cat)
                sub_cat.save()
            # Создаем объект Товара
            product = Product()
            product.shop = shop
            product.category = sub_cat
            product.save()
            # Создаем объект Описания Товара
            product_desc = ProductDescription(
                product=product,
                pack_type=temp_row[1].value.lower().rstrip(),
                art_mgb=temp_row[2].value,
                name=temp_row[3].value.lower().rstrip(),
                vat=temp_row[4].value,
                units=temp_row[5].value,
                price=temp_row[6].value,
                price_with_vat=temp_row[7].value,
                price_per_me=temp_row[8].value,
                price_per_me_with_vat=temp_row[9].value,

            )
            product_desc.save()
            continue
        # Если такое описание уже есть, то обновляем ему данные
        product_desc.vat = temp_row[4].value
        product_desc.units = temp_row[5].value
        product_desc.price = temp_row[6].value
        product_desc.price_with_vat = temp_row[7].value
        product_desc.price_per_me = temp_row[8].value
        product_desc.price_per_me_with_vat = temp_row[9].value
        product_desc.save()

    print("--- %s seconds ---" % (time.time() - start_time))


def fill_main_category():
    """
    Заполняем таблицу MainCategories
    :return:
    """
    from market.models import MainCategory
    categories = ["прочее", "для животных", "морепродукты", "овощи и фрукты", "мясо",
                  "полуфабрикаты", "бульоны / приправы / соусы", "масла", "чай, кофе и другое",
                  "мука и крупы", "кондитерские изделия", "напитки и соки", "молочная продукция",
                  "яйца", "алкогольная продукция", "косметика", "кухонная утварь", "бытовая химия",
                  "колбасные изделия", "птица", "рыба", "табачные изделия", "мебель",
                  "канцелярские принадлежности"]
    for category in sorted(categories):
        main_cat = MainCategory(name=category)
        main_cat.save()

    print('done')


def compare():
    """
    Заполняем таблицу SubCategories и строим связь с MainCategories
    :return:
    """
    from market.models import MainCategory, SubCategory
    file = os.path.join(BASE_DIR, 'media', 'cats.xlsx')
    wb = load_workbook(file)
    ws = wb[wb.sheetnames[1]]
    max_row = ws.max_row
    for row in ws.iter_rows(min_row=2, max_row=max_row):
        if row[2].value and row[0].value:
            main_cat = MainCategory.objects.filter(name=row[2].value.lower().rstrip()).first()
            if main_cat:
                cat = SubCategory()
                cat.category = main_cat
                cat.name = row[0].value.lower().rstrip()
                cat.save()
            else:
                continue
        else:
            continue
    print('done')


class string_with_title(str):
    # hack for app_label
    def __new__(cls, value, title):
        instance = str.__new__(cls, value)
        instance._title = title
        return instance

    def title(self):
        return self._title

    __copy__ = lambda self: self
    __deepcopy__ = lambda self, memodict: self